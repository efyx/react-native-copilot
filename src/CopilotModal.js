// @flow
import React, { Component } from 'react';
import { Animated, Easing, View, TouchableOpacity } from 'react-native';

import { Text, Button } from "native-base";
import styles, { MARGIN, ARROW_SIZE, STEP_NUMBER_DIAMETER, STEP_NUMBER_RADIUS } from './style';

type Props = {
  stop: () => void,
  next: () => void,
  prev: () => void,
  nextButton?: React$Element,
  prevButton?: React$Element,
  skipButton?: React$Element,
  finishButton?: React$Element,
  currentStepNumber: number,
  currentStep: ?Step,
  visible: boolean,
  isFirstStep: boolean,
  isLastStep: boolean,
};

type State = {
  tooltip: Object,
  arrow: Object,
  anim: Object,
  notAnimated: boolean,
};

class CopilotModal extends Component<Props, State> {
  static defaultProps = {
    nextStr: "Next",
    prevStr: "Previous",
    skipStr: "Stop",
    finishStr: "Finish",
  };

  state = {
    tooltip: {},
    arrow: {},
    anim: {
      leftOverlayRightBoundary: new Animated.Value(0),
      rightOverlayLeftBoundary: new Animated.Value(0),
      verticalOverlayLeftBoundary: new Animated.Value(0),
      verticalOverlayRightBoundary: new Animated.Value(0),
      topOverlayBottomBoundary: new Animated.Value(0),
      bottomOverlayTopBoundary: new Animated.Value(0),
      top: new Animated.Value(0),
      stepNumberLeft: new Animated.Value(0),
    },
    animated: false,
  };

  measure(): Promise {
    return new Promise((resolve, reject) => {
      this.wrapper.measure(
        (ox, oy, width, height, x, y) => resolve({
          x, y, width, height,
        }),
        reject,
      );
    });
  }

  async animateMove(obj = {}): void {
    const duration = 300;
    let stepNumberLeft = obj.left - STEP_NUMBER_RADIUS;

    const layout = await this.measure();

    if (stepNumberLeft < 0) {
      stepNumberLeft = (obj.left + obj.width) - STEP_NUMBER_RADIUS;
      if (stepNumberLeft > layout.width - STEP_NUMBER_DIAMETER) {
        stepNumberLeft = layout.width - STEP_NUMBER_DIAMETER;
      }
    }

    const center = {
      x: obj.left + (obj.width / 2),
      y: obj.top + (obj.height / 2),
    };

    const relativeToLeft = center.x;
    const relativeToTop = center.y;
    const relativeToBottom = Math.abs(center.y - layout.height);
    const relativeToRight = Math.abs(center.x - layout.width);

    const verticalPosition = relativeToBottom > relativeToTop ? 'bottom' : 'top';
    const horizontalPosition = relativeToLeft > relativeToRight ? 'left' : 'right';

    const tooltip = {};
    const arrow = {};

    if (verticalPosition === 'bottom') {
      tooltip.top = obj.top + obj.height + MARGIN;
      arrow.borderBottomColor = '#fff';
      arrow.top = tooltip.top - (ARROW_SIZE * 2);
    } else {
      tooltip.bottom = layout.height - (obj.top - MARGIN);
      arrow.borderTopColor = '#fff';
      arrow.bottom = tooltip.bottom - (ARROW_SIZE * 2);
    }

    if (horizontalPosition === 'left') {
      let tmp = Math.max(layout.width - (obj.left + obj.width), 0);
      tmp = tmp === 0 ? tmp + MARGIN : tmp;

      if (layout.width - (MARGIN * 2) > 500) {
        tooltip.right = tmp;
        tooltip.maxWidth = 500;
      } else {
        tooltip.left = MARGIN;
        tooltip.right = MARGIN;
      }

      arrow.right = tmp + MARGIN;
    } else {
      let tmp = Math.max(obj.left, 0);
      tmp = tmp === 0 ? tmp + MARGIN : tmp;

      if (layout.width - (MARGIN * 2) > 500) {
        tooltip.left = tmp;
        tooltip.maxWidth = 500;
      } else {
        tooltip.left = MARGIN;
        tooltip.right = MARGIN;
      }

      arrow.left = tmp + MARGIN;
    }

    const animate = {
      leftOverlayRightBoundary: layout.width - obj.left,
      rightOverlayLeftBoundary: obj.left + obj.width,
      verticalOverlayLeftBoundary: obj.left,
      verticalOverlayRightBoundary: layout.width - obj.left - obj.width,
      topOverlayBottomBoundary: layout.height - obj.top,
      bottomOverlayTopBoundary: obj.top + obj.height,
      top: obj.top,
      stepNumberLeft,
    };

    if (this.state.animated) {
      Animated.parallel(Object.keys(animate).map(key => Animated.timing(this.state.anim[key], {
        duration,
        toValue: animate[key],
        easing: Easing.linear,
      }))).start();
    } else {
      Object.keys(animate).forEach((key) => {
        this.state.anim[key].setValue(animate[key]);
      });
    }

    this.setState({
      tooltip,
      arrow,
      // FIXME: Animation is sluggish on Android
      // animated: true,
    });
  }

  render() {
    return this.props.visible ? (
      <View
        style={styles.container}
        ref={(element) => { this.wrapper = element; }}
        onLayout={() => { }}
      >
        <Animated.View
          style={[styles.overlayRectangle, { right: this.state.anim.leftOverlayRightBoundary }]}
        />
        <Animated.View
          style={[styles.overlayRectangle, { left: this.state.anim.rightOverlayLeftBoundary }]}
        />
        <Animated.View
          style={[
            styles.overlayRectangle,
            {
              top: this.state.anim.bottomOverlayTopBoundary,
              left: this.state.anim.verticalOverlayLeftBoundary,
              right: this.state.anim.verticalOverlayRightBoundary,
            },
          ]}
        />
        <Animated.View
          style={[
            styles.overlayRectangle,
            {
              bottom: this.state.anim.topOverlayBottomBoundary,
              left: this.state.anim.verticalOverlayLeftBoundary,
              right: this.state.anim.verticalOverlayRightBoundary,
            },
          ]}
        />

        <Animated.View style={[styles.arrow, this.state.arrow]} />
        <Animated.View style={[styles.tooltip, this.state.tooltip]}>
          <View style={{ flex: 1 }}>
            <Text style={styles.tooltipText}>{this.props.currentStep.text}</Text>
          </View>
          <View style={[styles.bottomBar]}>
            {
              !this.props.isLastStep ?
                <Button small bordered onPress={this.props.stop}>
                    <Text>{ this.props.skipStr }</Text>
                </Button>
                : null
            }
            {
              !this.props.isFirstStep ?
                <Button small bordered onPress={this.props.prev}>
                    <Text>{ this.props.prevStr }</Text>
                </Button>
                : null
            }
            {
              !this.props.isLastStep ?
                <Button small onPress={this.props.next}>
                    <Text style={ styles.buttonText }>{ this.props.nextStr }</Text>
                </Button> :
                <Button small onPress={this.props.stop}>
                    <Text style={ styles.buttonText }>{ this.props.finishStr }</Text>
                </Button>
            }
          </View>
        </Animated.View>
      </View>
    ) : null;
  }
}

export default CopilotModal;
